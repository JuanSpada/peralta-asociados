<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UserSeeder::class);
        // $blogs = factory(App\Blog::class, 10)->create();
        DB::table('users')->insert([
            'name' => 'Peralta y Asociados',
            'email' => 'rosanaperalta@peraltayasociados.com',
            'email_verified_at' => now(),
            'password' => Hash::make('wemanagement'), // password
            'remember_token' => Str::random(10),
        ]);
    }
}