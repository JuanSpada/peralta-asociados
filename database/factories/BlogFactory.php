<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Blog;
use Faker\Generator as Faker;

$factory->define(Blog::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'info' =>$faker->realText,
        'img' => 'default.png',
        'category' => rand(1, 4),
    ];
});
