-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 29-06-2021 a las 01:16:21
-- Versión del servidor: 10.1.48-MariaDB
-- Versión de PHP: 7.3.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `peralta-asociados`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `blogs`
--

INSERT INTO `blogs` (`id`, `category`, `title`, `info`, `img`, `created_at`, `updated_at`) VALUES
(12, 1, 'VIOLENCIA EN LIBERTAD REPRODUCTIVA', 'VIOLENCIA EN LIBERTAD REPRODUCTIVA. Diferencia con la violencia obstétrica. Jurisprudencia Nacional e Internacional. Ley de Natalidad.\r\nLa Ley 26485 de Protección Integral para Prevenir, Sancionar y Erradicar la violencia contra las mujeres en los ámbitos en que desarrollan sus relaciones interpersonales define:\r\nARTICULO 5. 3.- Sexual: Cualquier acción que implique la vulneración en todas sus formas, con o sin acceso genital, del derecho de la mujer de decidir voluntariamente acerca de su vida sexual o reproductiva a través de amenazas, coerción, uso de la fuerza o intimidación, incluyendo la violación dentro del matrimonio o de otras relaciones vinculares o de parentesco, exista o no convivencia, así como la prostitución forzada, explotación, esclavitud, acoso, abuso sexual y trata de mujeres.\r\nARTÍCULO 6º — Modalidades. A los efectos de esta ley se entiende por modalidades las formas en que se manifiestan los distintos tipos de violencia contra las mujeres en los diferentes ámbitos, quedando especialmente comprendidas las siguientes:\r\nd) Violencia contra la libertad reproductiva: aquella que vulnere el derecho de las mujeres a decidir libre y responsablemente el número de embarazos o el intervalo entre los nacimientos, de conformidad con la Ley 25.673 de Creación del Programa Nacional de Salud Sexual y Procreación Responsable;\r\nLa violencia sexual afecta la capacidad de un individuo para disfrutar de las relaciones sexuales.  La OPS ha reconocido que el abuso y la violencia sexual son problemas de salud reproductiva que afectan la calidad de vida, generan problemas emocionales y de conducta, y complican tanto el embarazo como el parto. A pesar que la violencia sexual genera un impacto en la salud sexual y reproductiva de las mujeres y desconoce sus derechos fundamentales, la gran mayoría de los países de la región no permiten el acceso al aborto en casos de violación. A pesar de las estadísticas y la evidencia, la mayoría de programas para prevenir y erradicar la violencia contra las mujeres no incorporan el acceso a servicios de salud sexual y reproductiva. La ausencia de políticas públicas al respecto está ligada a la criminalización de servicios básicos de salud sexual y reproductiva. La criminalización de estos servicios, exclusivos para las mujeres, no solamente reduce el rol de las mujeres a la maternidad sino también constituye una violación sistemática de sus derechos humanos, causándoles sufrimiento físico y psicológico. \r\nA pesar de los hechos mencionados anteriormente, las leyes y políticas de salud reproductiva de los países de América Latina y el Caribe son de las más restrictivas del mundo, particularmente con respecto al aborto y el acceso a la anticoncepción de emergencia. En términos generales, el aborto es legal en seis de los 33 países de América Latina y el Caribe. Es decir, es permitido independientemente de la razón por la cual se haya practicado o por razones socioeconómicas. En conjunto, estos países representan menos del 5% del total de las mujeres de la región, con edades entre los 15 y los 44 años. El 95% restante de las mujeres de edad fértil que habitan en la región, viven en países donde la ley de aborto es altamente restrictiva, incluyendo seis países en los cuales no se permite el aborto bajo ningún motivo, así sea para salvaguardar la vida de la mujer. Asimismo, en cinco países sólo es permitido para salvar la vida de la mujer y solamente en ocho países se permite el aborto en caso de violación o incesto. Debido a estas restricciones, los países de América Latina y el Caribe tienen la mayor tasa de aborto inseguro en el mundo, salvo por África Oriental.  De los 4,4 millones de abortos realizados en la región en el año 2008, el 95% (4,2 millones) fueron inseguros, incluyendo prácticamente todos los abortos realizados en América Central y América del Sur.37 A pesar que no existen datos disponibles después del 2008, de acuerdo con la tendencia, el número puede ser posiblemente mucho más alto. Cada año, cerca de un millón de mujeres en América Latina y el Caribe se encuentran hospitalizadas por complicaciones relacionadas con la práctica de un aborto inseguro.  El aborto inseguro constituye el 12% de todas las muertes maternas en América Latina y el Caribe. \r\ne) Violencia obstétrica: aquella que ejerce el personal de salud sobre el cuerpo y los procesos reproductivos de las mujeres, expresada en un trato deshumanizado, un abuso de medicalización y patologización de los procesos naturales, de conformidad con la Ley 25.929.\r\nEs interesante pensar que en términos de poder, la medicalización de un proceso natural abona la potestad del personal de la salud con la misma intensidad con que disminuye la soberanía de las mujeres sobre sus propios procesos reproductivos. Esto es: a mayor medicalización, menor poder para quien va a parir.\r\nRecordemos que históricamente el acto de parir fue un espacio exclusivo de mujeres y que  con los adelantos tecnológicos y las políticas higienistas del siglo pasado, es que florece la obstetricia y el saber médico termina de “expropiar” el cuerpo reproductor a las mujeres.\r\nQuizás la resistencia a reducir la asimetría propia de la relación médico-paciente, es lo que dificulta visibilizar como “violenta” la práctica sanitaria de “medicalizar” sin que exista una indicación precisa y justificada para ello. Debemos pensar que aplicar fórmulas predispuestas al acto de parir, por un lado homogeiniza a todas las “parturientas” bajo un mismo parámetro de mujer, y por el otro, favorece un modelo de atención biomédico que se enfoca de forma exclusivamente científica en la parte biológica del suceso y relega las particularidades, los tiempos y el deseo de cada mujer, que son las formas en que el personal de la salud debe tributar la  “autonomía” como derecho de todas las pacientes.\r\nNo hay partos normales. La “normalización” fue y es un arma poderosa para debilitar identidades y para deshumanizar, y precisamente de lo que trata la ley 25.929 de Parto Humanizado es de un parto humanizado y respetado donde el protagonismo –también el poder- es de quien va a parir.\r\nLa biología puesta en el centro de la relación médico-paciente y lo social en la periferia, es el esquema que sostiene el modelo médico-hegemónico (también llamado “asistencialista”) de atención sanitaria y, también, la piedra que obstaculiza terminar de migrar hacia un modelo de atención de la salud basado en derechos.\r\nPero por sobre todo, enfocarse en el sustrato biológico de la salud, es una de las tantas formas de la biopolítica foucaultiana que permite al personal de la salud obstétrica intervenir cuerpos, apropiarse de procesos vitales y disciplinar vidas de mujeres.\r\n\r\nCabe -asimismo- señalar que la violencia obstétrica es contemplada por tres normas de nuestro ordenamiento jurídico: 1. la precitada Ley 26.485 de Protección Integral; 2. la Ley 25.929 de Parto Respetado; y 3. la Ley 26.529 de Derechos del Paciente. Es decir que cuando no se cumple con los preceptos establecidos por la Ley de Parto Respetado, también se está incurriendo en una violencia obstétrica. La mujer es la protagonista del parto, y la ley le reconoce tres derechos fundamentales: a ser informada, a ser respetada y a ser considerada sana. Tiene el derecho a elegir el lugar y el proceso de parto, el acompañamiento, la posición, la analgesia, deambulación, libertad de movimiento, y contacto inmediato con el recién nacido.\r\nDesde una perspectiva de género, resulta central el análisis del control continuo de la sexualidad y la reproducción al que se ven sometidas las mujeres. En este sentido, Tamayo señala «la histórica y sistemática oposición de las jerarquías en el poder al reconocimiento de derechos y libertades en las esferas de la sexualidad y la reproducción puede ser observada en las más diversas modalidades». \r\n\r\nLa violencia obstétrica en cifras\r\nRecientemente, en el año 2016, el Observatorio de Violencia Obstétrica (OVO) dio a conocer las estadísticas que se obtuvieron a partir de la realización de encuestas voluntarias en su página web. Los datos muestran que la violencia obstétrica existe, afecta y violenta los derechos de muchísimas mujeres en todo el país, durante el parto, antes y/o después del mismo. La revista Cosmopolitan recogió algunos de los datos más resaltantes que dio a conocer el OVO:\r\n\r\nFalta de acompañamiento: Se deja a la mujer sola, sin interlocutores de confianza ni testigos de lo que le sucede en su internación. Durante el trabajo de parto, el 25% de las mujeres estuvieron totalmente solas. Durante el parto, un 36%. Durante la etapa de posparto, el 20%\r\n\r\nTrato: El 28% fue criticada por los médicos. Un 56% fue tratada con sobrenombres. Un 30% recibió comentarios irónicos o descalificadores. A un 36% las hicieron sentir que corrían peligro ella o su hijo/a Y un 47% no se sintió contenida.\r\n\r\nInformación: El 44% de las mujeres no fue debidamente informada sobre la evolución del trabajo de parto o su bienestar ni del de su bebé.\r\n\r\nCesárea: El 46% tuvo a su hijo por cesárea, siendo el porcentaje estándar de la OMS un 12-15%, de las cuales 39% fueron programadas. Dentro de este número, el 54% de las madres eran primerizas. Existe un mayor índice de cesáreas en el sistema privado, que se eleva hasta el 64%.\r\n\r\nRotura artificial de bolsa: El 70% de las encuestadas no recibió información clara, adecuada y completa y por ende no dio su autorización.\r\nInducción del parto. El 29, 7% de las encuestadas tuvo un parto inducido, siendo el porcentaje estándar de la OMS menor a 10%\r\n\r\nAnestesia: El 36% de las mujeres fueron anestesiadas sin haberlo solicitado.\r\n\r\nPrácticas sobre el/la bebé: El 74% de las mujeres no recibieron suficiente información sobre las prácticas que realizaron sobre su hijo/a, por ende tampoco dieron autorización para realizarlas. Aproximadamente el 45% de las mujeres no sabe o no recuerda qué prácticas fueron hechas sobre su hijo/a. \r\n\r\nLey de Natalidad\r\nLa planificación familiar permite a las personas tener el número de hijos que desean y determinar el intervalo entre embarazos. Se logra mediante la aplicación de métodos anticonceptivos y el tratamiento de la esterilidad (en esta nota se aborda solo la anticoncepción).\r\n\r\nBeneficios de la planificación familiar y de la anticoncepción\r\nLa promoción de la planificación familiar —y el acceso a los métodos anticonceptivos preferidos para las mujeres y las parejas— resulta esencial para lograr el bienestar y la autonomía de las mujeres y, al mismo tiempo, apoyar la salud y el desarrollo de las comunidades.\r\n\r\nPrevención de los riesgos para la salud relacionados con el embarazo en las mujeres.\r\nLa capacidad de la mujer para decidir si quiere embarazarse y en qué momento tiene una repercusión directa en su salud y bienestar. La planificación familiar permite espaciar los embarazos y puede posponerlos en las jóvenes que tienen mayor riesgo de morir por causa de la procreación prematura, lo cual disminuye la mortalidad materna. Evita los embarazos no deseados, incluidos los de mujeres de más edad, para quienes los riesgos ligados al embarazo son mayores. Permite además que las mujeres decidan el número de hijos que desean tener. Se ha comprobado que las mujeres que tienen más de cuatro hijos se enfrentan con un riesgo mayor de muerte materna.\r\nAl reducir la tasa de embarazos no deseados, la planificación familiar también disminuye la necesidad de efectuar abortos peligrosos.\r\n\r\nReducción de la mortalidad infantil\r\nLa planificación familiar puede evitar los embarazos muy cercanos entre sí y en un momento inoportuno, que contribuyen a causar algunas de las tasas de mortalidad infantil más elevadas del mundo. Las criaturas cuya madre muere a causa del parto también tienen un riesgo mayor de morir o enfermar.\r\n\r\nPrevención de la infección por el VIH y el SIDA\r\nLa planificación familiar disminuye el riesgo de que las mujeres infectadas por el VIH se embaracen sin desearlo, lo que da como resultado una disminución del número de criaturas infectadas y huérfanas. Además, los condones masculinos y femeninos brindan una protección doble: contra el embarazo no deseado y contra las infecciones de transmisión sexual, en especial la causada por el VIH.\r\n\r\nPoder de decisión y una mejor educación\r\nLa planificación familiar permite que las personas tomen decisiones bien fundamentadas con relación a su salud sexual y reproductiva. Brinda además la oportunidad de que las mujeres mejoren su educación y puedan participar más en la vida pública, en especial bajo la forma de empleo remunerado en empresas que no sean de carácter familiar. Tener una familia pequeña propicia que los padres dediquen más tiempo a cada hijo. Los niños que tienen pocos hermanos tienden a permanecer más años en la escuela que los que tienen muchos.\r\n\r\nDisminución del embarazo de adolescentes\r\nLas adolescentes que se embarazan tienen más probabilidades de dar a luz un niño de pretérmino o con peso bajo al nacer. Los hijos de las adolescentes presentan tasas más elevadas de mortalidad neonatal. Muchas adolescentes que se embarazan tienen que dejar la escuela, lo cual tiene consecuencias a largo plazo para ellas personalmente, para sus familias y para la comunidad.\r\n\r\nMenor crecimiento de la población\r\nLa planificación familiar es la clave para aminorar el crecimiento insostenible de la población y los efectos negativos que este acarrea sobre la economía, el medio ambiente y los esfuerzos nacionales y regionales por alcanzar el desarrollo.\r\n\r\n¿Quién presta servicios de planificación familiar y de la anticoncepción?\r\nEs importante que los servicios de planificación familiar estén ampliamente disponibles y sean de fácil acceso, por medio de parteras y otros agentes de salud capacitados, para toda persona sexualmente activa, en particular los adolescentes. Las parteras están capacitadas para facilitar (en los lugares en que estén autorizadas) los métodos anticonceptivos localmente disponibles y culturalmente aceptables.\r\n\r\nOtros agentes de salud calificados, por ejemplo, los agentes de salud comunitarios, también pueden facilitar asesoramiento y algunos métodos de planificación familiar, entre ellos píldoras y preservativos. Para métodos tales como la esterilización, tanto los hombres como las mujeres deben ser remitidos a un médico.\r\n\r\nUso de anticonceptivos\r\nEl uso de anticonceptivos ha aumentado en muchas partes del mundo, especialmente en Asia y América Latina, pero sigue siendo bajo en al África subsahariana. A escala mundial, el uso de anticonceptivos modernos ha aumentado ligeramente, de un 54% en 1990 a un 57,4% en 2015. A escala regional, la proporción de mujeres de entre 15 y 49 años de edad que usan algún método anticonceptivo ha aumentado mínimamente o se ha estabilizado entre 2008 y 2015. En África pasó de 23,6% a 28,5%; en Asia, el uso de anticonceptivos modernos ha aumentado ligeramente de un 60,9% a un 61,8%, y en América Latina y el Caribe el porcentaje ha permanecido en 66,7%.\r\n\r\nEl uso de métodos anticonceptivos por los hombres representa una proporción relativamente pequeña de las tasas de prevalencia mencionadas. Los métodos anticonceptivos masculinos se limitan al condón y la esterilización (vasectomía).\r\n\r\nLa OMS está trabajando para promover la planificación familiar mediante la preparación de directrices basadas en datos científicos sobre la seguridad de los métodos anticonceptivos y los servicios mediante los cuales se ofrecen; la preparación de normas de calidad y la precalificación de los productos anticonceptivos; y la ayuda a los países para que introduzcan, adapten y apliquen estos instrumentos para satisfacer sus necesidades. \r\n\r\nBIBLIOGRAFIA\r\n1)	Violencia en las Mujeres y Derechos Reproductivos en las Américas, Centro de Derechos Reproductivos, mayo 2015\r\n2)	“Violencia Obstétrica es violencia de Genero” de Perla Prigoshin. Microjuris.com, 24/1016.\r\n3)	“Callate y puja: porque la violencia obstétrica es violencia de genero” por Daniela Rey, www.economiafeminita.com 05/05/17.\r\n4)	Organización Mundial de la Salud. Julio 2017.\r\n\r\nDra. Rosana C. Peralta\r\nDNI 18153884', '12-img-1605189465.jpg', '2020-11-12 16:54:18', '2020-11-12 16:57:45'),
(13, 2, 'ESTAFA PROCESAL', 'DELITO DE ESTAFA PROCESAL\r\nNo hay duda de que la estafa, como tal, debe estar entre los delitos contra la propiedad, ya que no se castiga el engaño, como dice Finzi, sino el daño patrimonial que ocasiona, aunque el medio utilizado pueda causar daño a otro bien jurídico.\r\n                      En el Código Penal argentino el tipo de estafa se encuentra incluido en el Titulo 6, denominado Delitos contra la propiedad.  Sin embargo, basta con analizar el contenido de los diferentes tipos para reconocer que en realidad la protección legal va mucho más allá que el mero “derecho de propiedad”.\r\n                      Técnicamente resulta más adecuado hablar de “delitos contra el patrimonio”, pues no solo se incluyen acciones que lesionan o ponen en peligro la propiedad, sino también aquellas que afectan otros valores patrimoniales como la posesión, el derecho de crido, e incluso las expectativas. \r\n                        De esto se deriva –como pone de resalto Bajo Fernandez- que “el ataque a un elemento integrante del patrimonio (propiedad, posesión, derecho de crédito, etc.) solo podría constituir estafa cuando de él se derive una disminución del valor económico del patrimonio globalmente considerado, mientras que en otros tipos basta el ataque a dicho elemento patrimonial aislado para que se consume el delito, aun cuando el patrimonio, considerado unitariamente, reste incólume, o incluso beneficiado”.  \r\n                   El tipo objetivo de estafa exige la presencia de tres elementos fundamentales: fraude (ardid o engaño), error y disposición patrimonial perjudicial. Tales elementos deben darse en el orden descripto y vincularse por una relación de causalidad –o si se prefiere imputación objetiva- de modo tal que sea el fraude desplegado por el sujeto activo el que haya generado error en la victima y esta, en base a dicho error, realice una disposición patrimonial perjudicial.\r\n                      En principio deben ser idénticos el engañado y quien realiza la disposición patrimonial.  Pero a veces es posible, con relevancia penal, que en el hecho participen más personas y que esas dos partes sean distintas.  Entonces el perjuicio patrimonial no lo sufre la persona engañada, sino un tercero, que sería el titular del patrimonio.\r\n                       En la llamada estafa procesal, en la cual la victima del engaño es el juez, y el ofendido por la estafa la persona a quien afecta la sentencia o la resolución judicial dispositiva de la propiedad.  O en palabras de Muñoz Conde, se trata de que en un proceso la parte engaña al juez y esta dicta a consecuencia de un error una sentencia que causa perjuicio a la otra parte.\r\n                       Cerezo Mir afirma que de estafa procesal, en sentido estricto, cabe hablar solo “cuando una parte, con su conducta engañosa, realizada con ánimo lucro, induce a error al juez y éste, como consencuencia del error, dicta una sentencia injusta que causa un perjuicio patrimonial a la parte contraria o a un tercero” . Y esto solo es posible, según el autor citado, porque el engañado y el perjudicado son personas distintas.\r\n                      La disposición patrimonial del engañado debe influir en el propio patrimonio o en uno ajeno y a través de ello causar un daño o una disminución de ese patrimonio.  De modo que, para que el delito se perfeccione, el acto de disposición, provocado por el fraude-error, debe generar inevitablemente un perjuicio patrimonial en el propio sujeto engañado o en un tercero.\r\nDra. Rosana Peralta\r\nDNI 18153884', '13-img-1605189582.jpg', '2020-11-12 16:59:42', '2020-11-12 16:59:42');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_09_03_182832_create_blogs_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Peralta y Asociados', 'rosanaperalta@peraltayasociados.com', '2020-09-07 17:09:29', '$2y$10$fh05AGESi4umpXigUl9qJu2GFredIQLbWa288qTCRO09e/JP9do3G', 'fhPvt9Hjac', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
