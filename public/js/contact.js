var articleForm = document.querySelector('#contact');

articleForm.addEventListener('submit', function(event) {
    event.preventDefault();
    Swal.fire(
        '¡Enviado correctamente!',
        '¡Gracias por escribirnos, nos pondremos en contacto a brevedad!',
        'success'
        )
        let formData = new FormData(articleForm);
        articleForm.reset();
        fetch("/contact.php",
        {
            body: formData,
            method: "post"
        }
    );
});

var formSuscriber = document.querySelector('#suscriber');

formSuscriber.addEventListener('submit', function(event) {
    event.preventDefault();
    Swal.fire(
        '¡Enviado correctamente!',
        '¡Gracias por suscribirte a nuestro newsletter!',
        'success'
        )
        let formData = new FormData(formSuscriber);
        formSuscriber.reset();
        fetch("/suscriber.php",
        {
            body: formData,
            method: "post"
        }
    );
});