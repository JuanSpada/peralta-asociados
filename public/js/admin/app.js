
  
//Función para traer los datos de la ruleta
async function  getArticlesData() {
  let url = '/articles';
  try {
      let res = await fetch(url);
      return await res.json();
  } catch (error) {
      console.log(error);
  }
}

// Hay que hacer de esta manera las funciones para que pueda agarrar el promise.
async function renderArticlesData() { 
  let articlesData = await getArticlesData();
  articlesData.forEach(article => {
      article.settings = `<a href="admin/${article.id}">Editar</a> / <a href="admin/${article.id}/destroy">Eliminar</a>`;
      switch (article.category) {
        case 1:
          article.category = 'Violencia de Género'
          break;
        case 2:
          article.category = 'Delitos contra la propiedad'
          break;
        case 3:
          article.category = 'Delitos informáticos'
          break;
        case 4:
          article.category = 'Derecho Penal Corporativo'
          break;
      }
  });
  $('#blog-table').DataTable({
      data: articlesData,
      columns: [
          { data: 'title' },
          { data: 'category' },
          { data: 'info' },
          { data: 'settings' }
      ]
  });
}

renderArticlesData();