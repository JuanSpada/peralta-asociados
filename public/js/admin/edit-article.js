var toolbarOptions = [
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],
  
    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
    [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
    [{ 'direction': 'rtl' }],                         // text direction
  
    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  
    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'font': [] }],
    [{ 'align': [] }],
  
    ['clean']                                         // remove formatting button
  ];

let  quill = new Quill('#editor', {
    theme: 'snow',
    modules: {
        toolbar: toolbarOptions
    }
});

//Función para traer los datos de la ruleta
const articleId = document.querySelector('#aritcle_id').value;
async function  getArticleData() {
    let url = '/article/'+articleId;
    try {
        let res = await fetch(url);
        return await res.json();
    } catch (error) {
        console.log(error);
    }
}
  
// Hay que hacer de esta manera las funciones para que pueda agarrar el promise.
async function renderArticleData() { 
let articleData = await getArticleData();
    console.log(articleData);
    quill.setContents(JSON.parse(articleData.squillContents))
}
renderArticleData();

const infoInput = document.querySelector('#info');
const articleInput = document.querySelector('#article');
const squillContentInput = document.querySelector('#squillContent');
const editor = document.querySelector('#editor');
let articleForm = document.querySelector('#articleForm');
articleForm.addEventListener('submit', function(event) {
    event.preventDefault();
    squillContentInput.value = JSON.stringify(quill.getContents());
    infoInput.value = quill.getText().substring(0, 20);
    articleInput.value = quill.root.innerHTML;
    // console.log(infoInput.value)
    let formData = new FormData(articleForm);
    articleForm.reset();
    fetch("/admin/"+articleId,
    {
        body: formData,
        method: "post"
    }
    );
    window.location.href = "/admin"
});