var toolbarOptions = [
    ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
    ['blockquote', 'code-block'],
  
    [{ 'header': 1 }, { 'header': 2 }],               // custom button values
    [{ 'list': 'ordered'}, { 'list': 'bullet' }],
    [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
    [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
    [{ 'direction': 'rtl' }],                         // text direction
  
    [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
    [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
  
    [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
    [{ 'font': [] }],
    [{ 'align': [] }],
  
    ['clean']                                         // remove formatting button
  ];

let  quill = new Quill('#editor', {
    theme: 'snow',
    modules: {
        toolbar: toolbarOptions
    }
});

const infoInput = document.querySelector('#info');
const articleInput = document.querySelector('#article');
const squillContentInput = document.querySelector('#squillContent');
const editor = document.querySelector('#editor');
let articleForm = document.querySelector('#articleForm');
articleForm.addEventListener('submit', function(event) {
    event.preventDefault();
    infoInput.value = quill.getText().substring(0, 20);
    articleInput.value = quill.root.innerHTML;
    squillContentInput.value = JSON.stringify(quill.getContents());
    // console.log(infoInput.value)
    let formData = new FormData(articleForm);
    articleForm.reset();
    fetch("/admin/new",
    {
        body: formData,
        method: "post"
    }
    );
    window.location.href = "/admin"
});