<?php
// dd($request);
$url = "https://hooks.zapier.com/hooks/catch/803715/ol1r2hm/";


$name = $_POST['name'] ? $_POST['name'] : "";
$email = $_POST['email'] ? $_POST['email'] : "";
$phone = $_POST['phone'] ? $_POST['phone'] : "";
$message = $_POST['message'] ? $_POST['message'] : "";

$data = array(
    'name' => $name,
    'email' => $email,
    'phone' => $phone,
    'message' => $message,
);

$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
if ($result === FALSE) { }
