<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Http\Request;
use Validator;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::all();
        return view('admin.index')->with('blogs', $blogs);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        Validator::make($request->all(), [
            'title' => 'required|max:255',
            'category' => 'required|integer',
            'img' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'info' => 'required',
        ])->validate();

        $blog = new Blog;
        $blog->title = $request->title;
        $blog->category = $request->category;
        $blog->info = $request->info;
        $blog->article = $request->article;
        $blog->squillContents = $request->squillContent;
        $blog->img = "";
        $blog->save();
        if($request->hasFile('img')){
            $img = $request->file('img');
            $filename = $blog->id . '-' . 'img' . '-' . time() . '.' . $img->getClientOriginalExtension();
            $blog->img = $filename;
            $img->move(public_path('/storage/blog'), $filename);
        }
        $blog->save();
        return redirect('admin');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('admin.edit')->with('blog', $blog);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->request);
        Validator::make($request->all(), [
            'title' => 'required|max:255',
            'category' => 'required|integer',
            'img' => 'image|mimes:jpeg,png,jpg|max:2048',
            'info' => 'required',
        ])->validate();

        $blog = Blog::find($id);
        $blog->title = $request->title;
        $blog->category = $request->category;
        $blog->info = $request->info;
        $blog->article = $request->article;
        $blog->squillContents = $request->squillContent;
        $blog->img = "default.png";
        $blog->save();
        if($request->hasFile('img')){
            $img = $request->file('img');
            $filename = $blog->id . '-' . 'img' . '-' . time() . '.' . $img->getClientOriginalExtension();
            $blog->img = $filename;
            $img->move(public_path('/storage/blog'), $filename);
        }
        $blog->save();
        return redirect('admin');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::find($id);
        $blog->delete();
        return redirect('admin');
    }

    public function getArticlets()
    {
        $blogs = Blog::all();
        return response()->json($blogs, 200, []);
    }

    public function getArticle($id)
    {
        $blog = Blog::find($id);
        return response()->json($blog, 200, []);
    }
}
