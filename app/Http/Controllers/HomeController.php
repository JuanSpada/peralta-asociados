<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $blogs = Blog::orderBy('created_at', 'desc')->take(3)->get();
        return view('welcome')->with('blogs', $blogs);
    }

    public function blog()
    {
        $blogs = Blog::paginate(10);
        return view('blog.index')->with('blogs', $blogs);
    }

    public function showBlog($id)
    {
        $blogs = Blog::orderBy('created_at', 'desc')->take(3)->get();
        $article = Blog::find($id);
        return view('blog.show')->with('article', $article)->with('blogs', $blogs);
    }
}
