<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    public function showCategory(){
        switch ($this->category) {
            case '1':
                return 'Violencia de Género';
                break;
            case '2':
                return 'Delitos Contra la Propiedad';
                break;
            case '3':
                return 'Delitos Informáticos';
                break;
            case '4':
                return 'Derecho Penal Corporativo';
                break;
            default:
                return 'sin categoría';
                break;
        }
    }
}