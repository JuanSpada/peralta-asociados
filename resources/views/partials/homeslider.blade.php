<section id="home">
    <article class="content">			
        <div id="rev_slider_1083_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="travel" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- START REVOLUTION SLIDER 5.4.1 auto mode -->
            <div id="rev_slider_1083_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.4.1">
                <ul>	<!-- SLIDE  -->
                    <li data-index="rs-3070" data-transition="parallaxhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off"  data-title="Number One" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="/images/travel2.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                    </li>
                    <!-- SLIDE  -->
                    <li data-index="rs-3071" data-transition="parallaxhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-rotate="0"  data-saveperformance="off"  data-title="Number Two" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="/images/travel3.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                    </li>
                    <!-- SLIDE  -->
                    <li data-index="rs-3072" data-transition="parallaxhorizontal" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="default"  data-rotate="0"  data-saveperformance="off"  data-title="Number Three" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="/images/travel1.jpg"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                        <!-- LAYERS -->
                    </li>
                </ul>
                <div style="" class="tp-static-layers">
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption Travel-BigCaption   tp-resizeme tp-static-layer" 
                         id="slider-1083-layer-1" 
                         data-x="['middle','middle','middle','middle']" data-hoffset="['0','0','0','0']" 
                         data-y="['top','top','top','top']" data-voffset="['285','245','245','185']" 
                        data-fontsize="['90','90','60','35']"
                        data-lineheight="['95','95','65','45']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"

                        data-type="text" 
                        data-responsive_offset="on" 

                        data-startslide="0" 
                        data-endslide="3" 
                        data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                        data-textAlign="['center','center','center','center']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"

                        style="z-index: 5; white-space: nowrap;text-transform:center; font-weight: 400; font-family: 'EB Garamond', serif">Peralta & Asociados</div>

                    <!-- LAYER NR. 2 -->
                    <div class="tp-caption Travel-SmallCaption   tp-resizeme tp-static-layer" 
                         id="slider-1083-layer-2" 
                         data-x="['middle','middle','middle','middle']" data-hoffset="['0','0','0','0']" 
                         data-y="['top','top','top','top']" data-voffset="['403','363','313','253']" 
                        data-fontsize="['25','25','25','20']"
                        data-lineheight="['30','30','30','20']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"

                        data-type="text" 
                        data-responsive_offset="on" 

                        data-startslide="0" 
                        data-endslide="3" 
                        data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]'
                        data-textAlign="['left','left','left','left']"
                        data-paddingtop="[0,0,0,0]"
                        data-paddingright="[0,0,0,0]"
                        data-paddingbottom="[0,0,0,0]"
                        data-paddingleft="[0,0,0,0]"

                        style="z-index: 6; white-space: nowrap;text-transform:left;">Abogados</div>

                    <!-- LAYER NR. 3 -->
                    <div class="tp-caption Travel-CallToAction cs-button x-small  tp-static-layer" 
                         id="slider-1083-layer-3" 
                         data-x="['middle','middle','middle','middle']" data-hoffset="['0','0','0','0']" 
                         data-y="['top','top','top','top']" data-voffset="['466','426','376','301']" 
                                    data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"

                        data-type="button" 
                        data-actions='[{"event":"click","action":"scrollbelow","offset":"px","delay":""}]'
                        data-responsive_offset="on" 
                        data-responsive="off"
                        data-startslide="0" 
                        data-endslide="3" 
                        data-frames='[{"from":"opacity:0;","speed":300,"to":"o:1;","delay":500,"ease":"Power2.easeInOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"},{"frame":"hover","speed":"300","ease":"Power1.easeInOut","to":"o:1;rX:0;rY:0;rZ:0;z:0;","style":"c:rgba(255, 255, 255, 1.00);bg:rgba(255, 255, 255, 0.15);bw:2px 2px 2px 2px;"}]'
                        data-textAlign="['left','left','left','left']"
                        data-paddingtop="[12,12,12,12]"
                        data-paddingright="[20,20,20,20]"
                        data-paddingbottom="[12,12,12,12]"
                        data-paddingleft="[20,20,20,20]"

                        style="z-index: 7; white-space: nowrap;text-transform:left;outline:none;box-shadow:none;box-sizing:border-box;-moz-box-sizing:border-box;-webkit-box-sizing:border-box;cursor:pointer;">Más Información</div>
                </div>
                <div class="tp-bannertimer tp-bottom" style="height: 7px; background-color: rgba(255, 255, 255, 0.25);"></div>	
            </div>
        </div><!-- END REVOLUTION SLIDER -->

    </article>
</section>	