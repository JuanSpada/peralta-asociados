<!-- jQuery -->
<script src="/vendor/jquery/jquery.min.js"></script>

<!-- Popper -->
<script src="/vendor/js/popper.min.js"></script>

<!-- bootstrap Core JavaScript -->
<script src="/vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Corenav Master JavaScript -->
<script src="/vendor/corenav-master/coreNavigation-1.1.3.js"></script>
<script src="/js/nav.js"></script>

<!--carousel script -->
<script src="/vendor/owlcarousel/js/owl.carousel.min.js"></script>
<script src="/vendor/owlcarousel/js/jquery.mousewheel.min.js"></script>

<!-- custom JavaScript -->
<script src="/js/custom.js"></script>

<!-- REVOLUTION JS FILES -->
<script type="text/javascript" src="{{ asset('/vendor/revolution-slider/revolution/js/jquery.themepunch.tools.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendor/revolution-slider/revolution/js/jquery.themepunch.revolution.min.js') }}"></script>

<!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->	
<script type="text/javascript" src="{{ asset('/vendor/revolution-slider/revolution/js/extensions/revolution.extension.actions.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendor/revolution-slider/revolution/js/extensions/revolution.extension.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendor/revolution-slider/revolution/js/extensions/revolution.extension.kenburn.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendor/revolution-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendor/revolution-slider/revolution/js/extensions/revolution.extension.migration.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendor/revolution-slider/revolution/js/extensions/revolution.extension.navigation.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendor/revolution-slider/revolution/js/extensions/revolution.extension.parallax.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendor/revolution-slider/revolution/js/extensions/revolution.extension.slideanims.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('/vendor/revolution-slider/revolution/js/extensions/revolution.extension.video.min.js') }}"></script>
<script src="{{ asset('/js/revolution-slider.js') }}" type="text/javascript"></script>

<!--jarallax javascript -->
<script src="{{ asset('/js/jarallax.js') }}"></script>
<!-- Contact from script -->
<script src="/js/contact.js"></script>