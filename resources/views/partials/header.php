<header class="top-bar header-full-width">
    <div class="topbar">
      <div class="container-fluid">
        <div class="row justify-content-end">
          <div class="col-lg-6 col-md-6 col-12">
            <div class="topbar-call text-center text-md-left">
              <ul class="list-inline d-md-flex d-inline-block justify-content-start">
                 <li><i class="fa fa-phone theme-color mr-10"></i>+54 9 11 5153-8731</li>
                 <li class="px-10"><i class="fa fa-envelope-open-o theme-color mr-10"></i>info@peraltayasociados.com</li>
                 <li class="d-lg-inline-block d-none"><i class="fa fa-clock-o theme-color mr-10"></i> Lun - Vier 09:00-19:00</li>
              </ul>
            </div>
          </div>
          <div class="col-lg-6 col-md-6 col-12 xs-mb-10">
            <div class="topbar-social text-center text-md-right">
              <ul class="list-inline d-md-flex justify-content-end">
                <li><a href="https://www.facebook.com/PERALTA-Y-ASOCIADOSABOGADOS-227510857405499/"><span class="ti-facebook"></span></a></li>
                <li><a href="https://www.instagram.com/peraltayasociados.abogados/"><span class="ti-instagram"></span></a></li>
                <li><a href="https://twitter.com/AbogadosPeralta"><span class="ti-twitter-alt"></span></a></li>
                <li><a href="https://www.linkedin.com/in/rosana-peralta-52630318/"><span class="ti-linkedin"></span></a></li>
              </ul>
            </div>
          </div>				  
         </div>
      </div>
    </div>
    
    <nav hidden class="nav-white full-width nav-transparent">
        <div class="nav-header">
            <a href="/" class="brand">
                <img src="/images/brand/logo.png" style="width:160px" class="img-fluid" alt=""/>
            </a>
            <button class="toggle-bar">
                <span class="ti-menu"></span>
            </button>	
        </div>								
        <ul class="menu">
            <li class="scrollspy active"><a href="/">Inicio</a></li>
            <li class="scrollspy"><a href="/#about">Nosotros</a></li>
            <li class="scrollspy"><a href="/#practiceareas">Áreas de Práctica</a></li>
            <li class="scrollspy"><a href="/#testimonials">Testimonios</a></li>
            <li class="scrollspy"><a href="/#attorney">Abogados</a></li>
            <li class="scrollspy"><a href="/blog">Blog</a></li>
            <li class="scrollspy"><a href="/#contactus">Contacto</a></li>
        </ul>
    </nav>
</header>