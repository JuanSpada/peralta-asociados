<footer class="footer bg-black pt-50">

    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-sm-6">
                <h4 class="txt-white mb-30 mt-10 text-uppercase">Suscribirse</h4>
                <p class="mb-30">Suscríbase a nuestro Newsletter y reciba las últimas noticias.</p>
                <form action="" method="POST" name="suscriber" id="suscriber">
                    <input class="form-control placeholder" type="email" placeholder="Correo Electrónico" name="emailSuscriber" id="emailSuscriber" value="">
                    <div class="clear">
                        <button type="submit" class="cs-button cs-button-border white mt-20 form-button"> Enviar </button>
                    </div>
                </form>

            </div>
            <div class="col-lg-4 col-sm-6 xs-pt-30">
                <h4 class="text-white mb-30 mt-10 text-uppercase">Contacto</h4>
                <ul class="address">
                    <li><i class="fa fa-map-marker"></i>
                        <p>Avda. Alicia Moreau de Justo 1120 3º Piso Oficina 306 A de CABA</p>
                    </li>
                    <li><i class="fa fa-phone"></i>
                        <a href="tel:7042791249"> <span>52785922</span> </a>
                    </li>
                    <li><i class="fa fa-envelope-o"></i>Email: info@peraltayasociados.com.ar</li>
                </ul>
            </div>
            <div class="col-lg-2 col-sm-6 sm-mt-30">
                <div class="footer-link footer-link-hedding">
                    <h4 class="txt-white mb-30 mt-10 text-uppercase">Links rápidos</h4>
                    <ul>
                        <li><a href="#home">Inicio</a></li>
                        <li><a href="#about">Áreas de Práctica</a></li>
                        <li><a href="#practiceareas">Testimonios</a></li>
                        <li><a href="#testimonials">Abogados</a></li>
                        <li><a href="#attorney">Blog</a></li>
                        <li><a href="#contactus">Contacto</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-2 col-sm-6 sm-mt-30">
                <div class="footer-link footer-link-hedding">
                    <h4 class="txt-white mb-30 mt-10 text-uppercase">Blog</h4>
                    <ul>
                        <li><a href="/blog">Violencia de Género</a></li>
                        <li><a href="/blog">Delitos contra la propiedad</a></li>
                        <li><a href="/blog">Delitos Informáticos</a></li>
                        <li><a href="/blog">Derecho Penal Corporativo</a></li>
                    </ul>
                </div>
            </div>
        </div>
        
        <div class="row">
            <div class="col-md-10 mt-50">
                <div class="footer-link footer-link-hedding">
                    <h4 class="txt-white text-uppercase">Áreas de Práctica</h4>
                    <ul>
                        <div class="row" style="width:100vw">
                            <div class="col-md-3">
                                <li><a href="#practiceareas">Delitos conra la vida</a></li>
                                <li><a href="#practiceareas">Delitos contra la integridad sexual</a></li>
                                <li><a href="#practiceareas">Delitos contra la propiedad privada</a></li>
                                <li><a href="#practiceareas">Delitos contra el honor</a></li>
                                <li><a href="#practiceareas">Delitos contra el orden público</a></li>
                            </div>
                            <div class="col-md-3">
                                <li><a href="#practiceareas">Delitos contra la fe pública</a></li>
                                <li><a href="#practiceareas">Mala Praxis</a></li>
                                <li><a href="#practiceareas">Ley de Estupefacientes</a></li>
                                <li><a href="#practiceareas">Fraudes corporativos</a></li>
                                <li><a href="#practiceareas">Lavado de dinero</a></li>
                            </div>
                            <div class="col-md-3">
                                <li><a href="#practiceareas">Penal Tributario y Previsional</a></li>
                                <li><a href="#practiceareas">Penal Aduanero y Contrabando</a></li>
                                <li><a href="#practiceareas">Penal Cambiario</a></li>
                                <li><a href="#practiceareas">Corrupción</a></li>
                                <li><a href="#practiceareas">Cibercrimen</a></li>
                                <li><a href="#practiceareas">Penal Ambiental</a></li>
                                <li><a href="#practiceareas">Fuero contravencional y de faltas</a></li>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="footerbox">
                    <div class="content">
                        <h4 class="text-white">¿Estás buscando un Abogado?</h4>
                        <p>Estas en el lugar indicado, ponete en contacto con nosotros. </p>
                    </div>
                    <div class="link">
                        <a class="cs-button cs-button-border white small" href="#contactus">Contactanos</a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="footerbox sm-mt-10">
                    <div class="content">
                        <h4 class="text-white">¿Querés que evaluemos tu caso?</h4>
                        <p>Somos especialistas en lo que hacemos, podemos asesorarte en todo el proceso. </p>
                    </div>
                    <div class="link">
                        <a class="cs-button cs-button-border white small" href="#contactus">Contactanos</a>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="footer-copyright mt-20">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <p class="mt-15"> ©Copyright <span id="copyright">2020</span> <a href="#"> Peralta & Asociados </a> Todos los derechos reservados </p>
                    <p>By <a href="https://www.wemanagement.com.ar">We Management</a></p>
                </div>
                <div class="col-lg-6 col-md-6 text-left text-md-right">
                    <div class="social-icons color-hover mt-10">
                        <ul> 
                            <li class="social-facebook"><a href="https://www.facebook.com/PERALTA-Y-ASOCIADOSABOGADOS-227510857405499/"><i class="fa fa-facebook"></i></a></li>
                            <li class="social-twitter"><a href="https://twitter.com/AbogadosPeralta"><i class="fa fa-twitter"></i></a></li>
                            <li class="social-dribbble"><a href="https://www.instagram.com/peraltayasociados.abogados/"><i class="fa fa-instagram"></i> </a></li>
                            <li class="social-dribbble"><a href="https://www.linkedin.com/in/rosana-peralta-52630318/"><i class="fa fa-linkedin"></i> </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>