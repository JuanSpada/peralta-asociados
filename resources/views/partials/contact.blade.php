<section id="contactus" class="section-ptb-80 bg-overlay-black-90" data-jarallax='{"speed": 0.0}' style="background-image: url(/images/bg-img.jpg);">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="section-heading line center text-center">
					<h1 class="heading text-white">Contactanos</h1>
				</div>
			</div>
		</div>
		<div class="row mt-50">
			<div class="col-12">
				<form class="mt-30" action="" id="contact" name="contact" method="POST">
					@csrf
					<div class="contact-form form-border clearfix">
						<div class="form-field">
							<input type="text" class="form-control" name="name" id="name" placeholder="Nombre*" required>
						</div>
						<div class="form-field">
							<input type="email" class="form-control" name="email" id="email" placeholder="Email*" required>
						</div>
						<div class="form-field">
							<input type="text" class="form-control" name="phone" id="phone" placeholder="Teléfono*" required>
						</div>
						<div class="form-field textarea">
							<textarea class="form-control input-message" placeholder="Mensaje*" rows="7" name="message" id="message"></textarea>
						</div>
						<div class="form-field submit-button w-100">
							<button name="submit" type="submit" class="cs-button"> Enviar Mensaje </button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</section>