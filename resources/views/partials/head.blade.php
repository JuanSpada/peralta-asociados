<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">
    
    <title>Peralta & Asociados</title>

    <!-- plugins -->
    {{-- <link href="{{ asset('/vendor/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet"> --}}

    <link rel="stylesheet" type="text/css" href="{{ asset('/css/plugin.css') }}" />
    
    <!-- Revolution Slider -->		
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/revolution-slider/revolution/fonts/pe-icon-7-stroke/css/pe-icon-7-stroke.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/revolution-slider/revolution/css/settings.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/revolution-slider/revolution/css/layers.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('/vendor/revolution-slider/revolution/css/navigation.css') }}">        

    <!-- typography -->
    {{-- {{ asset('') }} --}}
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/typographies.css') }}" />

    <!-- templete element -->
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/template-element.css') }}" />        

    <!-- template CSS -->
    <link href="{{ asset('/css/style.css') }}" rel="stylesheet">

    <!-- responsive CSS -->
    <link href="{{ asset('/css/responsive.css') }}" rel="stylesheet">

     <!-- custom CSS -->
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet">

    <!-- SWEET ALERT -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
</head>