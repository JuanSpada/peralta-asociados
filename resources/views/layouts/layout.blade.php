<!DOCTYPE html>
<html lang="es">
    @include('partials/head')

    <body>
        @include('partials/header')
        
        <!-- Start Slider -->
        @include('partials/homeslider')
        <!-- End Slider -->
        
        @yield('content')
        
    
        <section class="section-ptb-0 bg-theme xs-py-30">
            <div class="container">
                <div class="row align-items-center">				
                    <div class="col-12">
                        <div class="d-md-flex justify-content-start align-items-center d-block text-md-left text-center">
                            <div class="xs-text-center">
                                <img src="/images/img-hold.png" alt="" class="img-fluid" />
                            </div>
                            <div class="text-center px-40 xs-my-30">
                                <h2 class="text-white f-w4 mb-0">Impulsar su caso es de lo que somos responsables.</h2>
                            </div>
                            <div>							
                                <a class="cs-button cs-button-border white large d-block w-100" href="#">Contactanos</a>
                            </div>						
                        </div>
                    </div>
                </div>				
            </div>
        </section>
        
        @include('partials/footer')
        <div class="btn-whatsapp">
            <a href="https://api.whatsapp.com/send?phone=5491151538731" target="_blank">
            <img src="/images_new/btn_whatsapp.png" alt="">
            </a>
        </div>
        
    </body>
    
    @include('partials/scripts')
    
	
</html>