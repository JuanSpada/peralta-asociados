@extends('admin/layouts/layout')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Artículos</h1>
    </div>
    <section class="row">
        <div class="col-md-12">
            <table id="blog-table" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th>Título</th>
                        <th>Categoría</th>
                        <th>Contenido</th>
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    {{-- @foreach ($blogs as $blog)
                        <tr>
                            <td>{{$blog->title}}</td>
                            <td>{{$blog->showCategory()}}</td>
                            <td>{{$blog->info}}</td>
                            <td><a href="{{route('admin.edit', $blog->id)}}">Editar</a> / <a href="{{route('admin.destroy', $blog->id)}}">Eliminar</a></td>
                        </tr>
                    @endforeach --}}
                </tbody>
                <tfoot>
                    <tr>
                        <th>Título</th>
                        <th>Categoría</th>
                        <th>Contenido</th>
                        <th>Acciones</th>
                   </tr>
                </tfoot>
            </table>
        </div>
    </section>
</main>
@endsection