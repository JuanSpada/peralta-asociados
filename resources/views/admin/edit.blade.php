@extends('admin/layouts/layout')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Editar Artículo "{{$blog->title}}"</h1>
    </div>
    <section class="row">
        <div class="col-md-12">
            <form method="POST" action="" id="articleForm" enctype="multipart/form-data">
                <input type="text" hidden id="aritcle_id" value="{{$blog->id}}">
                @csrf
                <div class="form-group col-md-6">
                    <label for="title">Título:*</label>
                    <input @error('title') is-invalid @enderror required type="text" class="form-control" id="title" name="title" value="{{$blog->title}}">
                </div>
                <div class="form-group col-md-6">
                    <label for="category">Categoría:*</label>
                    <select @error('category') is-invalid @enderror required class="form-control" id="category" name="category">
                        <option selected disabled>Seleccionar</option>
                        <option @if($blog->category == 1) selected="selected" @endif value="1">Violencia de Género</option>
                        <option @if($blog->category == 2) selected="selected" @endif value="2">Delitos contra la propiedad</option>
                        <option @if($blog->category == 3) selected="selected" @endif value="3">Delitos informáticos</option>
                        <option @if($blog->category == 4) selected="selected" @endif value="4">Derecho Penal Corporativo</option>
                    </select>
                </div>
                <div class="form-group col-md-10">
                    <label for="img">Imagen:* (Tamaño recomendado 600x600)</label>
                    <input @error('img') is-invalid @enderror type="file" id="img" name="img">
                </div>
                <img src="/storage/blog/{{$blog->img}}" style="max-width:600px; max-height:600px;" alt="{{$blog->title}}">
                <div class="form-group col-md-10">
                    <label for="info">Texto:*</label>
                    <div id="editor" style="height: 450px; margin-bottom: 20px"></div>

                    {{-- <textarea @error('info') is-invalid @enderror required class="form-control" id="info" name="info" rows="16">{{$blog->info}}</textarea> --}}
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif  
                <input type="text" hidden name="info" id="info">
                <input type="text" hidden name="article" id="article">
                <input type="text" hidden name="squillContent" id="squillContent">
                <button class="btn btn-primary" type="submit" style="width:220px; margin-bottom:20px">Cargar</button>
            </form>
        </div>
    </section>
</main>
   
<script src="{{ asset('js/admin/edit-article.js') }}" defer></script>
@endsection