@extends('admin/layouts/layout')
@section('content')

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-md-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Cargar Artículo</h1>
    </div>
    <section class="row">
        <div class="col-md-12">
            <form method="POST" id="articleForm" action="{{route('admin.store')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group col-lg-6">
                    <label for="title">Título:*</label>
                    <input @error('title') is-invalid @enderror required type="text" class="form-control" id="title" name="title">
                </div>
                <div class="form-group col-lg-6">
                    <label for="category">Categoría:*</label>
                    <select @error('category') is-invalid @enderror required class="form-control" id="category" name="category">
                        <option selected disabled>Seleccionar</option>
                        <option value="1">Violencia de Género</option>
                        <option value="2">Delitos contra la propiedad</option>
                        <option value="3">Delitos informáticos</option>
                        <option value="4">Derecho Penal Corporativo</option>
                    </select>
                </div>
                <div class="form-group col-md-10">
                    <label for="img">Imagen:* (Tamaño recomendado 600x600)</label>
                    <input @error('img') is-invalid @enderror type="file" id="img" name="img" required>
                </div>
                <div class="form-group col-md-10">
                    <label for="info">Texto:*</label>
                    {{-- <textarea @error('info') is-invalid @enderror required class="form-control" id="info" name="info" rows="16"></textarea> --}}
                </div>
                <input type="text" hidden name="info" id="info">
                <input type="text" hidden name="article" id="article">
                <input type="text" hidden name="squillContent" id="squillContent">
                <div id="editor" style="height: 450px; margin-bottom: 20px"></div>
                    
                </div>
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif  
                <button class="btn btn-primary" type="submit" style="width:220px; margin-bottom:20px">Cargar</button>
            </form>
        </div>
    </section>
</main>
<script src="{{ asset('js/admin/new-article.js') }}" defer></script>
@endsection