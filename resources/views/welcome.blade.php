@extends('layouts/layout')
@section('content')
<section id="about" class="section-pb-80 sm-pb-40">
	<div class="container">
		<div class="about-bx">
			<div class="row no-gutter align-items-center">
				<div class="col-lg-6 col-12">
					<div class="px-70 sm-px-20 sm-py-20">
						<div class="section-heading text-left">
							<h2 class="heading f-w4"><span class="theme-color">Más de 30</span> años de experiencia</h2>
							<h3>DUM SPIRAMUS TUEBIMUR</h3>
							<p>Mientras respiremos defenderemos:</p>
						</div>
						<p>Brindamos servicios jurídicos de excelencia. Antes de tomar una decisión respecto a su caso, consúltenos  para ser asistido de inmediato por especialistas del derecho.  Nuestra área de cobertura se extiende a tribunales de todo el país y juzgados internacionales.</p>
					</div>
				</div>
				<div class="col-lg-6 col-12">
					<div>
						<img src="/images/lawyer.jpg" class="img-fluid" alt="" />
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="section-pb-80 sm-pb-40">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-12">
				<div class="feature-txt img-icon left-icon">
					<div class="feature-icon">
						<img src="/images/icon-3.png" class="img-fluid" alt="" />
					</div>
					<div class="feature-info">
						<h3 class="f-w5 mb-20">Solicite un abogado</h3>
						{{-- <p>Tus derechos, nuestro trabajo.</p> --}}
					</div>
				</div>						
			</div>
			<div class="col-md-4 col-12">						
				<div class="feature-txt img-icon left-icon">
					<div class="feature-icon">
						<img src="/images/icon-2.png" class="img-fluid" alt="" />
					</div>
					<div class="feature-info">
						<h3 class="f-w5 mb-20">Investigación de caso</h3>
						{{-- <p>Investigamos su caso como si fuese</p> --}}
					</div>
				</div>
			</div>	
			<div class="col-md-4 col-12">
				<div class="feature-txt img-icon left-icon">
					<div class="feature-icon">
						<img src="/images/icon-1.png" class="img-fluid" alt="" />
					</div>
					<div class="feature-info">
						<h3 class="f-w5 mb-20">Directorio de búsqueda</h3>
						{{-- <p>This proposal is a win-win situation which they cause a stellar paradigm shift, and produce a multi-fold increase in deliverables. </p> --}}
					</div>
				</div>
			</div>												
		</div>				
	</div>
</section>		

<section class="section-ptb-80 bg-overlay-black-40" data-jarallax='{"speed": 0.0}' style="background-image: url(/images/bg-img-2.jpg);">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-8">
				<div class="section-heading line center text-center">
					<h1 class="heading text-white">¿Por qué elegirnos?</h1>
				</div>
			</div>
		</div>
		<div class="row mt-40 mb-80">
			<div class="col-md-6 col-12">
				<div>
					<p class="text-white-70">El Estudio Jurídico se dedica al ejercicio profesional del Derecho Penal en sus distintas ramas. La actividad la desarrollamos en la Ciudad Autónoma de Buenos Aires, la Provincia de Buenos Aires y el Fuero Federal de todo el país.</p>
				</div>
			</div>
			<div class="col-md-6 col-12">
				<div>
					<p class="text-white-70">Los 30 años de experiencia en el ámbito privado y de organismos no gubernamentales (ONGs) nos permite desarrollar diversos servicios de consultoría y asistencia técnica en todo tipo de causas penales.</p>
				</div>
			</div>
		</div>
		{{-- <div class="row">
			<div class="col-md-4 col-12 xs-mb-30 text-center">
				<div class="counter text-white">
					<h1 class="f-w6 white">$<span class="count">52300</span></h1>
					<img src="/images/did.png" class="img-fluid my-20" alt="" />
					<p class="text-white fs-20 f-w5">Recovered For Our Client</p>
				</div>
			</div>
		
			<div class="col-md-4 col-12 xs-mb-30 text-center">
				<div class="counter text-white">
					<h1 class="f-w6 white">$<span class="count">36400</span></h1> 
					<img src="/images/did.png" class="img-fluid my-20" alt="" />
					<p class="text-white fs-20 f-w5">Recovered For Client This Year</p>
				</div>
			</div>
			<div class="col-md-4 col-12 text-center">
				<div class="counter text-white">
					<h1 class="f-w6 white">$<span class="count">21400</span></h1>
					<img src="/images/did.png" class="img-fluid my-20" alt="" />
					<p class="text-white fs-20 f-w5">Average Saved Per Cases</p>
				</div>
			</div>
		</div> --}}
	</div>
</section>

<section id="practiceareas" class="section-pt-80 pb-40">
	<div class="container-fluid">
		<div class="row justify-content-center">
			<div class="col-md-5 col-12">
				<div class="section-heading line center text-center">
					<h1 class="heading">Nos especializamos en</h1>
				</div>
			</div>
		</div>
		
		<div class="row mt-50">
			<div class="col-12">
				<div class="owl-carousel owl-theme" data-nav-dots="true"  data-items="5" data-lg-items="4" data-md-items="3" data-sm-items="2" data-xs-items="1" data-xx-items="1">
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Delitos Contra la Vida.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Delitos contra la vida</h2>
									{{-- <p class="text-white">Homicidios, femicidios, o muertes por accidentes de tránsito, entre otros.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Delito Integridad Sexual.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Delitos contra la integridad sexual</h2>
									{{-- <p class="text-white">Abusos sexuales, corrupción de menores, trata de personas, entre otros.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Delito contra la Propiedad Privada.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Delitos contra la propiedad privada</h2>
									{{-- <p class="text-white">Hurtos, robos en todas sus modalidades, extorsión, defraudaciones y estafas, daños, entre otros.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Delito contra el honor.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Delitos contra el honor</h2>
									{{-- <p class="text-white">Calumnia e injurias, entre otros.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Delito contra el orden publico.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Delitos contra el orden público</h2>
									{{-- <p class="text-white">Asociación ilícita, instigación a cometer delitos, entre otros.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Delito Mala Praxis.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Mala Praxis</h2>
									{{-- <p class="text-white">Responsabilidad penal por mala praxis de médicos, y otras profesiones.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Ley de Estupefacientes.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Ley de Estupefacientes</h2>
									{{-- <p class="text-white">Tenencia para consumo, comercio, deposito, entre otros</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Extradicion.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Extradiciones</h2>
									{{-- <p class="text-white">Fraude y recupero de activos, administración fraudulenta, “empleado infiel”, investigaciones internas con protocolos de actuación.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Fraude Corporativo.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Fraudes corporativos</h2>
									{{-- <p class="text-white">Fraude y recupero de activos, administración fraudulenta, “empleado infiel”, investigaciones internas con protocolos de actuación.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Lavado de Dinero.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Lavado de dinero</h2>
									{{-- <p class="text-white">Fraude y recupero de activos, administración fraudulenta, “empleado infiel”, investigaciones internas con protocolos de actuación.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Penal Tributario.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Penal Tributario y Previsional</h2>
									{{-- <p class="text-white">Fraude y recupero de activos, administración fraudulenta, “empleado infiel”, investigaciones internas con protocolos de actuación.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Penal Aduanero.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Penal Aduanero y Contrabando</h2>
									{{-- <p class="text-white">Fraude y recupero de activos, administración fraudulenta, “empleado infiel”, investigaciones internas con protocolos de actuación.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Penal Cambiario.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Penal Cambiario</h2>
									{{-- <p class="text-white">Fraude y recupero de activos, administración fraudulenta, “empleado infiel”, investigaciones internas con protocolos de actuación.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Delito Corrupcion.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Corrupción</h2>
									{{-- <p class="text-white">Fraude y recupero de activos, administración fraudulenta, “empleado infiel”, investigaciones internas con protocolos de actuación.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Cibercrimen.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Cibercrimen</h2>
									{{-- <p class="text-white">Fraude y recupero de activos, administración fraudulenta, “empleado infiel”, investigaciones internas con protocolos de actuación.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Delito Penal Ambiental.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Penal Ambiental</h2>
									{{-- <p class="text-white">Fraude y recupero de activos, administración fraudulenta, “empleado infiel”, investigaciones internas con protocolos de actuación.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
					<div class="item">
						<div class="deals-box">
							<figure class="effect-goliath">
								<img src="/images/areas/Fuero Contravencional.png" alt="">
								<figcaption>
									<h2 class="text-white text-left">Fuero Penal contravencional y de faltas</h2>
									{{-- <p class="text-white">Fraude y recupero de activos, administración fraudulenta, “empleado infiel”, investigaciones internas con protocolos de actuación.</p> --}}
								</figcaption>			
							</figure>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="testimonials" class="section-ptb-80">
	<div class="container">
		<div class="testimonials-border">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="section-heading line center text-center">
						<h1 class="heading">Testimonios</h1>
					</div>
				</div>
			</div>
			<div class="row mt-50 justify-content-center">
				<div class="col-lg-9 col-12">
					<div class="owl-carousel owl-theme" data-nav-dots="true"  data-items="1" data-md-items="1" data-sm-items="1" data-xs-items="1" data-xx-items="1">
						<div class="item">
							<div class="testimonials">
							  <div class="testimonials-avatar"> <img alt="" src="/images/testimonials/valeria cifuentes.jpg"> </div>
							  <div class="testimonials-info"> El estudio jurídico defendió mis derechos en sede Contravencional Penal y de Faltas de la CABA, salí sobreseída ante la causa iniciada por mis empleadores y además ganamos el juicio laboral</div>
							  <div class="author-info"> <strong>Valeria Cifuentes</strong> </div>
							</div>
						</div>
						<div class="item">
							<div class="testimonials">
							  <div class="testimonials-avatar"> <img alt="" src="/images/testimonials/daniel cristodero.jpg"> </div>
							  <div class="testimonials-info"> El estudio jurídico me asesora en todo lo concerniente a mi Pyme y también en cuestiones personales. Me siento muy acompañado.</div>
							  <div class="author-info"> <strong>Daniel Cristodero</strong> </div>
							</div>
						</div>
						<div class="item">
							<div class="testimonials">
							  <div class="testimonials-avatar"> <img alt="" src="/images/testimonials/miguel cabral.jpg"> </div>
							  <div class="testimonials-info"> El estudio jurídico me defendió en el Fuero Federal de la Nación y mi abogada me asesoró de manera que fuí sobreseído ante la denuncia del Registro de la Propiedad Automotor.</div>
							  <div class="author-info"> <strong>Miguel Cabral</strong> </div>
							</div>
						</div>
						<div class="item">
							<div class="testimonials">
							  <div class="testimonials-avatar"> <img alt="" src="/images/testimonials/Daniel Rodolfo Ruffo.jpg"> </div>
							  <div class="testimonials-info"> El estudio juridico me representó en un juicio ante el Banco Frances y lo ganamos, me sentí muy acompañado por los letrados que integran el estudio.</div>
							  <div class="author-info"> <strong>Daniel Ruffo</strong> </div>
							</div>
						</div>
						<div class="item">
							<div class="testimonials">
							  <div class="testimonials-avatar"> <img alt="" src="/images/testimonials/Andrea Bursztyn.jpg"> </div>
							  <div class="testimonials-info"> El estudio jurídico me defendió en el Fuero Federal de la Nación y fui sobreseída.  Si bien la causa duró bastante tiempo los letrados siempre me asesoraron y me acompañaron en todos los pasos procesales.</div>
							  <div class="author-info"> <strong>Andrea Burzstyn</strong> </div>
							</div>
						</div>
						<div class="item">
							<div class="testimonials">
							  <div class="testimonials-avatar"> <img alt="" src="/images/testimonials/silvia herrera.jpg"> </div>
							  <div class="testimonials-info"> El estudio juridico me está acompañando en una acción frente a la Superintendencia de Riesgos de Trabajo y me asesoraron y me acompañan en todo momento.</div>
							  <div class="author-info"> <strong>Silvia Herrera</strong> </div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="attorney" class="section-ptb-80 bg-light">
	<div class="container">
		<div class="row mt-50">
			<div class="col-12">
				<div class="section-heading line center text-center">
					<h1 class="heading">Abogados</h1>
				</div>
			</div>
		</div>
		<div class="row d-flex justify-content-center">
			<div class="col-lg-3 col-md-6 col-12">
				<div class="team team-round-small">
					<div class="team-photo">
						<img class="img-fluid mx-auto" src="/images/team/rosana.jpg" alt="">
					</div>
					<div class="team-description">
						<div class="team-info">
							<h4><a href="team-single.html"> Dra. Rosana Peralta</a></h4>
							<span>Abogada</span>
						</div>
						{{-- <div class="team-contact">
							<p><i class="fa fa-thumbs-up theme-color"></i> 1544 Cases</p>
						</div> --}}
						<div class="social-icons social-border rounded color-hover clearfix">
							<ul>
								<li class="social-facebook"><a href="https://www.facebook.com/rosana.peralta1/"><i class="fa fa-facebook"></i></a></li>
								<li class="social-twitter"><a href="https://www.instagram.com/rosanaperalta3/"><i class="fa fa-instagram"></i></a></li>
								<li class="social-linkedin"><a href="https://www.linkedin.com/feed/"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>                        
			</div>
		</div>
		<div class="row">
			<div class="col-lg-3 col-md-6 col-12">
				<div class="team team-round-small">
					<div class="team-photo">
						<img class="img-fluid mx-auto" src="/images/team/alfredo.jpg" alt="">
					</div>
					<div class="team-description">
						<div class="team-info">
							<h4><a href="team-single.html"> Dr. Alfredo Uro</a></h4>
							<span>Abogado</span>
						</div>
						{{-- <div class="team-contact">
							<p><i class="fa fa-thumbs-up theme-color"></i> 1544 Cases</p>
						</div> --}}
						<div class="social-icons social-border rounded color-hover clearfix">
							<ul>
								<li class="social-facebook"><a href="https://www.facebook.com/monica.burlon"><i class="fa fa-facebook"></i></a></li>
								<li class="social-twitter"><a href="https://www.instagram.com/monicaburlon/"><i class="fa fa-instagram"></i></a></li>
								<li class="social-linkedin"><a href="https://www.linkedin.com/in/monica-burlon-1ba8a924/"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>                        
			</div>
			<div class="col-lg-3 col-md-6 col-12">
				<div class="team team-round-small">
					<div class="team-photo">
						<img class="img-fluid mx-auto" src="/images/team/monica.jpg" alt="">
					</div>
					<div class="team-description">
						<div class="team-info">
							<h4><a href="team-single.html"> Dra. Mónica Emma Burlon</a></h4>
							<span>Abogada</span>
						</div>
						{{-- <div class="team-contact">
							<p><i class="fa fa-thumbs-up theme-color"></i> 1544 Cases</p>
						</div> --}}
						<div class="social-icons social-border rounded color-hover clearfix">
							<ul>
								<li class="social-facebook"><a href="https://www.facebook.com/monica.burlon"><i class="fa fa-facebook"></i></a></li>
								<li class="social-twitter"><a href="https://www.instagram.com/monicaburlon/"><i class="fa fa-instagram"></i></a></li>
								<li class="social-linkedin"><a href="https://www.linkedin.com/in/monica-burlon-1ba8a924/"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>                        
			</div>
			<div class="col-lg-3 col-md-6 col-12">
				<div class="team team-round-small">
					<div class="team-photo">
						<img class="img-fluid mx-auto" src="/images/team/cristian.jpg" alt="">
					</div>
					<div class="team-description">
						<div class="team-info">
							<h4><a href="team-single.html"> Dr. Cristian Rene Lafont</a></h4>
							<span>Abogado</span>
						</div>
						{{-- <div class="team-contact">
							<p><i class="fa fa-thumbs-up theme-color"></i> 1544 Cases</p>
						</div> --}}
						<div class="social-icons social-border rounded color-hover clearfix">
							<ul>
								<li class="social-facebook"><a href="https://www.facebook.com/lafontcristian"><i class="fa fa-facebook"></i></a></li>
								<li class="social-twitter"><a href="https://www.instagram.com/lafont.cristian/"><i class="fa fa-instagram"></i></a></li>
								<li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div>
					</div>
				</div>                        
			</div>
			<div class="col-lg-3 col-md-6 col-12">
				<div class="team team-round-small">
					<div class="team-photo">
						<img class="img-fluid mx-auto" src="/images/team/incognito.png" alt="">
					</div>
					<div class="team-description">
						<div class="team-info">
							<h4><a href="team-single.html"> Antonio Britez</a></h4>
							<span>Detective Privado</span>
						</div>
						{{-- <div class="team-contact">
							<p><i class="fa fa-thumbs-up theme-color"></i> 1544 Cases</p>
						</div> --}}
						{{-- <div class="social-icons social-border rounded color-hover clearfix">
							<ul>
								<li class="social-facebook"><a href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-twitter"><a href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-linkedin"><a href="#"><i class="fa fa-linkedin"></i></a></li>
							</ul>
						</div> --}}
					</div>
				</div>                        
			</div>
		</div>
	</div>
</section>


@include('partials/contact')


<section id="blog" class="section-ptb-80 bg-light-gold">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="section-heading line center text-center">
					<h1 class="heading">Blog</h1>
				</div>
			</div>
		</div>
		<div class="row mt-50">
			@foreach ($blogs as $blog)
				<div class="col-lg-4 col-md-4">
					<div class="blog-post xs-mb-50">
						<div class="entry-image clearfix">
							<img class="img-fluid" style="height: 350px; width:350px;" src="/storage/blog/{{$blog->img}}" alt="{{$blog->title}}">
						</div>
						<div class="blog-detail">
							<div class="entry-title mb-10">
								<a href="blog/{{$blog->id}}">{{$blog->title}}</a>
							</div>
							<div class="entry-meta mb-10">
								<ul>
									<li><a href="blog/{{$blog->id}}"><i class="fa fa-calendar-o"></i>{{$blog->updated_at->diffForHumans()}}</a></li>
								</ul>
							</div>
							<div class="entry-content">
								<p>{{ substr($blog->info, 0,  20) }}...</p>
							</div>
							<div class="entry-share clearfix">
								<div class="entry-button">
									<a class="cs-button arrow" href="blog/{{$blog->id}}">Ver más<i class="fa fa-angle-right" aria-hidden="true"></i></a>
								</div>
								{{-- <div class="social list-style-none float-right">
									<strong>Share : </strong>
									<ul>
										<li>
											<a href="blog/{{$blog->id}}"> <i class="fa fa-facebook"></i> </a>
										</li>
										<li>
											<a href="blog/{{$blog->id}}"> <i class="fa fa-twitter"></i> </a>
										</li>
									</ul>
								</div> --}}
							</div>
						</div>
					</div>
				</div>
			@endforeach
			{{-- <div class="col-lg-4 col-md-4">
				<div class="blog-post xs-mb-50">
					<div class="entry-image clearfix">
						<img class="img-fluid" src="/images/blog-02.jpg" alt="">
					</div>
					<div class="blog-detail">
						<div class="entry-title mb-10">
							<a href="#">Legal issues regarding paternity</a>
						</div>
						<div class="entry-meta mb-10">
							<ul>
								<li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2019</a></li>
							</ul>
						</div>
						<div class="entry-content">
							<p>In this Kidnapping the unlawful taking away or transportation of person against that person's will unlawfully,</p>
						</div>
						<div class="entry-share clearfix">
							<div class="entry-button">
								<a class="cs-button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
							</div>
							<div class="social list-style-none float-right">
								<strong>Share : </strong>
								<ul>
									<li>
										<a href="#"> <i class="fa fa-facebook"></i> </a>
									</li>
									<li>
										<a href="#"> <i class="fa fa-twitter"></i> </a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="blog-post">
					<div class="entry-image clearfix">
						<img class="img-fluid" src="/images/blog-03.jpg" alt="">
					</div>
					<div class="blog-detail">
						<div class="entry-title mb-10">
							<a href="#">Judgement, Unfair business</a>
						</div>
						<div class="entry-meta mb-10">
							<ul>
								<li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2019</a></li>
							</ul>
						</div>
						<div class="entry-content">
							<p>In this Kidnapping the unlawful taking away or transportation of person against that person's will unlawfully,</p>
						</div>
						<div class="entry-share clearfix">
							<div class="entry-button">
								<a class="cs-button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
							</div>
							<div class="social list-style-none float-right">
								<strong>Share : </strong>
								<ul>
									<li>
										<a href="#"> <i class="fa fa-facebook"></i> </a>
									</li>
									<li>
										<a href="#"> <i class="fa fa-twitter"></i> </a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div> --}}
		</div>
	</div>
</section>
@endsection