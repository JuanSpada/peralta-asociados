@extends('layouts/layout')
@section('content')

<section id="blog" class="section-ptb-80 bg-light-gold">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="section-heading line center text-center">
					<h1 class="heading">Blog</h1>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<ul class="nav nav-pills mb-3 d-flex justify-content-center" id="pills-tab" role="tablist">
					<li class="nav-item" role="presentation">
					  <a class="nav-link active" id="pills-1-tab" data-toggle="pill" href="#pills-1" role="tab" aria-controls="pills-1" aria-selected="true">Violencia de Género</a>
					</li>
					<li class="nav-item" role="presentation">
					  <a class="nav-link" id="pills-2-tab" data-toggle="pill" href="#pills-2" role="tab" aria-controls="pills-2" aria-selected="false">Delitos contra la propiedad</a>
					</li>
					<li class="nav-item" role="presentation">
					  <a class="nav-link" id="pills-3-tab" data-toggle="pill" href="#pills-3" role="tab" aria-controls="pills-3" aria-selected="false">Delitos Informáticos</a>
					</li>
					<li class="nav-item" role="presentation">
						<a class="nav-link" id="pills-4-tab" data-toggle="pill" href="#pills-4" role="tab" aria-controls="pills-4" aria-selected="false">Derecho Penal Corporativo</a>
					  </li>
				  </ul>
			</div>
		</div>

		<div class="tab-content" id="pills-tabContent">
			<div class="tab-pane fade show active" id="pills-1" role="tabpanel" aria-labelledby="pills-1-tab">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12">
							<div class="section-heading line center text-center">
								<h1 class="heading">Lo último</h1>
							</div>
						</div>
					</div>
					<div class="row mt-50">
						@foreach ($blogs as $blog)
							@if ($blog->category == 1)
								<div class="col-lg-4 col-md-4 mb-50">
									<div class="blog-post xs-mb-50">
										<div class="entry-image clearfix">
											<img class="img-fluid" src="/storage/blog/{{$blog->img}}" alt="{{$blog->title}}">
										</div>
										<div class="blog-detail">
											<div class="entry-title mb-10">
												<a href="{{route('blog.show', $blog->id)}}">{{$blog->title}}</a>
											</div>
											<div class="entry-meta mb-10">
												<ul>
													<li><a href="{{route('blog.show', $blog->id)}}"><i class="fa fa-calendar-o"></i> {{$blog->updated_at->diffForHumans()}}</a></li>
												</ul>
											</div>
											<div class="entry-content">
												<p>{{ substr($blog->info, 0,  20) }}...</p>
											</div>
											<div class="entry-share clearfix">
												<div class="entry-button">
													<a class="cs-button arrow" href="{{route('blog.show', $blog->id)}}">Ver más<i class="fa fa-angle-right" aria-hidden="true"></i></a>
												</div>
												{{-- <div class="social list-style-none float-right">
													<strong>Share : </strong>
													<ul>
														<li>
															<a href="{{route('blog.show', $blog->id)}}"> <i class="fa fa-facebook"></i> </a>
														</li>
														<li>
															<a href="#"> <i class="fa fa-twitter"></i> </a>
														</li>
													</ul>
												</div> --}}
											</div>
										</div>
									</div>
								</div>
							@endif
						@endforeach
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="pills-2" role="tabpanel" aria-labelledby="pills-2-tab">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12">
							<div class="section-heading line center text-center">
								<h1 class="heading">Lo último</h1>
							</div>
						</div>
					</div>
					<div class="row mt-50">
						@foreach ($blogs as $blog)
							@if ($blog->category == 2)
								<div class="col-lg-4 col-md-4 mb-50">
									<div class="blog-post xs-mb-50">
										<div class="entry-image clearfix">
											<img class="img-fluid" style="height: 350px; width:350px;" src="/storage/blog/{{$blog->img}}" alt="{{$blog->title}}">
										</div>
										<div class="blog-detail">
											<div class="entry-title mb-10">
												<a href="{{route('blog.show', $blog->id)}}">{{$blog->title}}</a>
											</div>
											<div class="entry-meta mb-10">
												<ul>
													<li><a href="{{route('blog.show', $blog->id)}}"><i class="fa fa-calendar-o"></i> {{$blog->updated_at->diffForHumans()}}</a></li>
												</ul>
											</div>
											<div class="entry-content">
												<p>{{ substr($blog->info, 0,  20) }}...</p>
											</div>
											<div class="entry-share clearfix">
												<div class="entry-button">
													<a class="cs-button arrow" href="{{route('blog.show', $blog->id)}}">Ver más<i class="fa fa-angle-right" aria-hidden="true"></i></a>
												</div>
												{{-- <div class="social list-style-none float-right">
													<strong>Share : </strong>
													<ul>
														<li>
															<a href="{{route('blog.show', $blog->id)}}"> <i class="fa fa-facebook"></i> </a>
														</li>
														<li>
															<a href="#"> <i class="fa fa-twitter"></i> </a>
														</li>
													</ul>
												</div> --}}
											</div>
										</div>
									</div>
								</div>
							@endif
						@endforeach
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="pills-3" role="tabpanel" aria-labelledby="pills-3-tab">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12">
							<div class="section-heading line center text-center">
								<h1 class="heading">Lo último</h1>
							</div>
						</div>
					</div>
					<div class="row mt-50">
						@foreach ($blogs as $blog)
							@if ($blog->category == 3)
								<div class="col-lg-4 col-md-4 mb-50">
									<div class="blog-post xs-mb-50">
										<div class="entry-image clearfix">
											<img class="img-fluid" style="height: 350px; width:350px;" src="/storage/blog/{{$blog->img}}" alt="{{$blog->title}}">
										</div>
										<div class="blog-detail">
											<div class="entry-title mb-10">
												<a href="{{route('blog.show', $blog->id)}}">{{$blog->title}}</a>
											</div>
											<div class="entry-meta mb-10">
												<ul>
													<li><a href="{{route('blog.show', $blog->id)}}"><i class="fa fa-calendar-o"></i> {{$blog->updated_at->diffForHumans()}}</a></li>
												</ul>
											</div>
											<div class="entry-content">
												<p>{{ substr($blog->info, 0,  20) }}...</p>
											</div>
											<div class="entry-share clearfix">
												<div class="entry-button">
													<a class="cs-button arrow" href="{{route('blog.show', $blog->id)}}">Ver más<i class="fa fa-angle-right" aria-hidden="true"></i></a>
												</div>
												{{-- <div class="social list-style-none float-right">
													<strong>Share : </strong>
													<ul>
														<li>
															<a href="{{route('blog.show', $blog->id)}}"> <i class="fa fa-facebook"></i> </a>
														</li>
														<li>
															<a href="#"> <i class="fa fa-twitter"></i> </a>
														</li>
													</ul>
												</div> --}}
											</div>
										</div>
									</div>
								</div>
							@endif
						@endforeach
					</div>
				</div>
			</div>
			<div class="tab-pane fade" id="pills-4" role="tabpanel" aria-labelledby="pills-4-tab">
				<div class="container">
					<div class="row justify-content-center">
						<div class="col-12">
							<div class="section-heading line center text-center">
								<h1 class="heading">Lo último</h1>
							</div>
						</div>
					</div>
					<div class="row mt-50">
						@foreach ($blogs as $blog)
							@if ($blog->category == 4)
								<div class="col-lg-4 col-md-4 mb-50">
									<div class="blog-post xs-mb-50">
										<div class="entry-image clearfix">
											<img class="img-fluid" src="/storage/blog/{{$blog->img}}" alt="{{$blog->title}}">
										</div>
										<div class="blog-detail">
											<div class="entry-title mb-10">
												<a href="{{route('blog.show', $blog->id)}}">{{$blog->title}}</a>
											</div>
											<div class="entry-meta mb-10">
												<ul>
													<li><a href="{{route('blog.show', $blog->id)}}"><i class="fa fa-calendar-o"></i> {{$blog->updated_at->diffForHumans()}}</a></li>
												</ul>
											</div>
											<div class="entry-content">
												<p>{{ substr($blog->info, 0,  20) }}...</p>
											</div>
											<div class="entry-share clearfix">
												<div class="entry-button">
													<a class="cs-button arrow" href="{{route('blog.show', $blog->id)}}">Ver más<i class="fa fa-angle-right" aria-hidden="true"></i></a>
												</div>
												{{-- <div class="social list-style-none float-right">
													<strong>Share : </strong>
													<ul>
														<li>
															<a href="{{route('blog.show', $blog->id)}}"> <i class="fa fa-facebook"></i> </a>
														</li>
														<li>
															<a href="#"> <i class="fa fa-twitter"></i> </a>
														</li>
													</ul>
												</div> --}}
											</div>
										</div>
									</div>
								</div>
							@endif
						@endforeach
					</div>
				</div>
			</div>
		  </div>
	</div>
</section>

@include('partials/contact')
@endsection