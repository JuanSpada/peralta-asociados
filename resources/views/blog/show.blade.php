@extends('layouts/layout')
@section('content')

<section id="about" class="section-pb-80 sm-pb-40">
	<div class="container">
		<div class="about-bx">
			<div class="row no-gutter align-items-center">
				<div class="col-lg-6 col-12">
					<div class="px-70 sm-px-20 sm-py-20">
						<div class="section-heading text-left">
							<h2 class="heading f-w4"><span class="theme-color">Más de 30</span> años de experiencia <br>DUM SPIRAMUS TUEBIMUR </h2>
							<p>Mientras respiremos defenderemos:</p>
						</div>
						<p>El Estudio Jurídico se dedica al ejercicio profesional del Derecho Penal en sus distintas ramas. La actividad la desarrollamos en la Ciudad Autónoma de Buenos Aires, la Provincia de Buenos Aires y el Fuero Federal de todo el país. Los 30 años de experiencia en el ámbito privado y de organismos no gubernamentales (ONGs) nos permite desarrollar diversos servicios de consultoría y asistencia técnica en todo tipo de causas penales.</p>
					</div>
				</div>
				<div class="col-lg-6 col-12">
					<div>
						<img src="/images/lawyer.jpg" class="img-fluid" alt="" />
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<div class="container d-flex justify-content-center">
	<img src="/storage/blog/{{$article->img}}" class="img-fluid mt-10" alt="{{$article->title}}" style="border-radius: 8px;">
</div>
<div class="container">

	<section class="section-ptb-80">
		<div class="row d-flex justify-content-center " style="max-width:100vw;">
			<div class="col-md-10">
				<div class="section-heading line center text-center">
					<h1 class="heading">{{$article->title}}</h1>
				</div>
				<p class="mb-5 text-center" style="color:#9b7b30;">{{$article->showCategory()}}</p>
				{!! $article->article !!}
			</div>
		</div>
	</section>
</div>
<div class="col-md-6">
</div>

<section id="blog" class="section-ptb-80 bg-light-gold">
	
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-12">
				<div class="section-heading line center text-center">
					<h1 class="heading">Lo último</h1>
				</div>
			</div>
		</div>
		<div class="row mt-50">
			@foreach ($blogs as $blog)
				<div class="col-lg-4 col-md-4">
					<div class="blog-post xs-mb-50">
						<div class="entry-image clearfix">
							<img class="img-fluid" style="height: 350px; width:350px;" src="/storage/blog/{{$blog->img}}" alt="{{$blog->title}}">
						</div>
						<div class="blog-detail">
							<div class="entry-title mb-10">
								<a href="{{route('blog.show', $blog->id)}}">{{$blog->title}}</a>
							</div>
							<div class="entry-meta mb-10">
								<ul>
									<li><a href="{{route('blog.show', $blog->id)}}"><i class="fa fa-calendar-o"></i> {{$blog->updated_at->diffForHumans()}}</a></li>
								</ul>
							</div>
							<div class="entry-content">
								<p>{{ substr($blog->info, 0,  20) }}...</p>
							</div>
							<div class="entry-share clearfix">
								<div class="entry-button">
									<a class="cs-button arrow" href="{{route('blog.show', $blog->id)}}">Ver más<i class="fa fa-angle-right" aria-hidden="true"></i></a>
								</div>
								{{-- <div class="social list-style-none float-right">
									<strong>Share : </strong>
									<ul>
										<li>
											<a href="{{route('blog.show', $blog->id)}}"> <i class="fa fa-facebook"></i> </a>
										</li>
										<li>
											<a href="#"> <i class="fa fa-twitter"></i> </a>
										</li>
									</ul>
								</div> --}}
							</div>
						</div>
					</div>
				</div>
			@endforeach
			{{-- <div class="col-lg-4 col-md-4">
				<div class="blog-post xs-mb-50">
					<div class="entry-image clearfix">
						<img class="img-fluid" src="/images/blog-02.jpg" alt="">
					</div>
					<div class="blog-detail">
						<div class="entry-title mb-10">
							<a href="#">Legal issues regarding paternity</a>
						</div>
						<div class="entry-meta mb-10">
							<ul>
								<li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2019</a></li>
							</ul>
						</div>
						<div class="entry-content">
							<p>In this Kidnapping the unlawful taking away or transportation of person against that person's will unlawfully,</p>
						</div>
						<div class="entry-share clearfix">
							<div class="entry-button">
								<a class="cs-button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
							</div>
							<div class="social list-style-none float-right">
								<strong>Share : </strong>
								<ul>
									<li>
										<a href="#"> <i class="fa fa-facebook"></i> </a>
									</li>
									<li>
										<a href="#"> <i class="fa fa-twitter"></i> </a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="blog-post">
					<div class="entry-image clearfix">
						<img class="img-fluid" src="/images/blog-03.jpg" alt="">
					</div>
					<div class="blog-detail">
						<div class="entry-title mb-10">
							<a href="#">Judgement, Unfair business</a>
						</div>
						<div class="entry-meta mb-10">
							<ul>
								<li><a href="#"><i class="fa fa-calendar-o"></i> 12 Aug 2019</a></li>
							</ul>
						</div>
						<div class="entry-content">
							<p>In this Kidnapping the unlawful taking away or transportation of person against that person's will unlawfully,</p>
						</div>
						<div class="entry-share clearfix">
							<div class="entry-button">
								<a class="cs-button arrow" href="#">Read More<i class="fa fa-angle-right" aria-hidden="true"></i></a>
							</div>
							<div class="social list-style-none float-right">
								<strong>Share : </strong>
								<ul>
									<li>
										<a href="#"> <i class="fa fa-facebook"></i> </a>
									</li>
									<li>
										<a href="#"> <i class="fa fa-twitter"></i> </a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div> --}}
		</div>
	</div>
</section>

@include('partials/contact')
@endsection