<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/artisan', 'ArtisanController@artisan')->name('artisan');

Auth::routes();
Route::get('/', 'HomeController@index')->name('index');
Route::prefix('/blog')->name('blog.')->group(function() {
    Route::get('/', 'HomeController@blog')->name('blog');
    Route::get('/{id}', 'HomeController@showBlog')->name('show');
});
Route::middleware(['auth'])->group(function() {
    Route::prefix('/admin')->name('admin.')->group(function() {
        Route::get('/', 'BlogController@index')->name('index');
        Route::get('/new', 'BlogController@create')->name('create');
        Route::post('/new', 'BlogController@store')->name('store');
        Route::get('/{id}', 'BlogController@edit')->name('edit');
        Route::post('/{id}', 'BlogController@update')->name('update');
        Route::get('/{id}/destroy', 'BlogController@destroy')->name('destroy');
    });
});

Route::get('/articles', 'BlogController@getArticlets')->name('getArticlets');
Route::get('/article/{id}', 'BlogController@getArticle')->name('getArticle');
